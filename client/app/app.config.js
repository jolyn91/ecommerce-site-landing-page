(function () {
    angular
        .module("TechTest")
        .config(MyAppConfig);
    MyAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function MyAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/home',
                views: {
                    "header": {
                        templateUrl: "app/home/1header.html"
                    },
                    "navigation_menu": {
                        templateUrl: "app/home/2navigation_menu.html"
                    },
                    "banner_slider": {
                        templateUrl: "app/home/3banner_slider.html"
                    },
                    "banner_images": {
                        templateUrl: "app/home/4banner_images.html"
                    },
                    "featured_items": {
                        templateUrl: "app/home/5featured_items.html"
                    },
                    "footer": {
                        templateUrl: "app/home/6footer.html"
                    }
                },
                controller: 'HomePageAppCtrl',
                controllerAs: 'ctrl'
            });

        $urlRouterProvider.otherwise("/home");
    }

})();

