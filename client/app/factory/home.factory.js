(function () {

    angular.module('TechTest')
        .factory('HomeFactory', HomeFactory);


    console.log("-- in home.factory.js");
    HomeFactory.$inject = ["$q", "$http"];

    function HomeFactory($q, $http) {

        var vm = this;

        console.log("in HomeFactory function");

        // return available functions for use in the controllers
        return ({
            getShopItems: getShopItems,
            getFeaturedImages: getFeaturedImages
        });


        function getShopItems() {
            console.log('getMenuItems');

            var deferred = $q.defer();
            $http.get('app/home/shop.json')
            // handle success
                .then(function (data) {
                    console.log("--this is the data: " + JSON.stringify(data));
                    deferred.resolve(data);
                })
                .catch(function (err) {
                    deferred.reject(err);
                    console.log("--this is error: " + JSON.stringify(err));
                });
            return deferred.promise;
        }

        function getFeaturedImages() {
            console.log('getFeaturedImages');

            var deferred = $q.defer();
            $http.get('app/home/products.json')
            // handle success
                .then(function (data) {
                    console.log("--this is the data: " + JSON.stringify(data));
                    deferred.resolve(data);
                })
                .catch(function (err) {
                    deferred.reject(err);
                    console.log("--this is error: " + JSON.stringify(err));
                });
            return deferred.promise;
        }

    }

})();