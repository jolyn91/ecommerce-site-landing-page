(function () {
    angular
        .module("TechTest")
        .controller("HomePageAppCtrl", ["HomeFactory", HomePageAppCtrl]);

    console.log("In home controller");
    function HomePageAppCtrl(HomeFactory) {
        var vm = this;

        vm.shopItems = {};
        vm.featuredItems = [];

        initMenuItems();
        initFeaturedItems();

        function initMenuItems() {
            console.info("-- home.contr.js > initMenuItems()");
            HomeFactory.getShopItems()
                .then(function (response) {
                    console.log("--response data in initMenuItems(): " + JSON.stringify(response.data));
                    vm.shopItems = response.data;
                })
                .catch(function (err) {
                    console.error("Error initing menu items!")
                })
        }

        function initFeaturedItems() {
            console.info("-- home.contr.js > initFeaturedImages()");
            HomeFactory.getFeaturedImages()
                .then(function (response) {
                    console.log("--response data in initFeaturedImages(): " + JSON.stringify(response.data));
                    vm.featuredItems = [];
                    vm.featuredItems = response.data;
                })
                .catch(function (err) {
                    console.error("Error initing featured images!")
                })
        }
    }
})();